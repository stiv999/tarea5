#Autor: Steven Guaman A.            #stiveng9991@gmail.com

'''Calcular si el número ingresado es primo'''

def num_prim (num):
    if num < 1:
        return False
    elif num == 2:
        return True
    else:
        for i in range(2, num):
            if num % i == 0:
                return False
        return True

if __name__ == '__main__':
    num = int (input('Introduzca un número:'))
    r = num_prim(num)
    if r is True:
        print (f'{num}\tes número primo')
    else:
        print (f'{num}\tno es número primo')


